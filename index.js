const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const jwt = require('jsonwebtoken')
const { CustomError } = require('accommerce-helpers')

module.exports = (userModel, secretKey) => {
    // Local auth
    passport.use(new LocalStrategy(userModel.authenticate()))
    passport.serializeUser(userModel.serializeUser())
    passport.deserializeUser(userModel.deserializeUser())

    // JWT auth
    const options = {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: secretKey,
    }

    passport.use(
        new JwtStrategy(options, (jwt_payload, done) => {
            console.log('JWT payload:', jwt_payload)

            userModel.findById(jwt_payload._id, (err, user) => {
                if (err) return done(err, false)
                if (user) return done(null, user)
                return done(null, false)
            })
        })
    )

    const _getToken = (user) => {
        return jwt.sign(user, secretKey, { expiresIn: 604800 })
    }

    const _createToken = (req, res, next) => {
        passport.authenticate('local', (err, user, info) => {
            if (err) return next(err)

            if (!user) {
                return next(
                    CustomError(
                        `Tài khoản hoặc mật khẩu không chính xác`,
                        `Invalid username or password`,
                        401
                    )
                )
            }

            if (user.deletedAt) {
                return next(CustomError(`Người dùng đã bị xóa`, `User has been deleted!`, 401))
            }

            if (!user.isActive) {
                return next(
                    CustomError(
                        `Người dùng tạm thời bị vô hiệu. Vui lòng liên hệ Admin để biết thêm thông tin chi tiết`,
                        `User is temporarily inactive. Please contact Admin for more information`,
                        401
                    )
                )
            }

            // Load user info to request
            req.logIn(user, (err) => {
                if (err) {
                    return next(err)
                }
            })

            const token = _getToken({ _id: req.user._id })
            res.status(200).json({
                success: true,
                token: 'Bearer ' + token,
                message: 'Login successfully!',
            })
        })(req, res, next)
    }

    const _verifyLogin = (req, res, next) => {
        passport.authenticate('jwt', { session: false }, (err, user, info) => {
            if (err) {
                return next(err)
            }

            if (!user) {
                return next(
                    CustomError(
                        `Tài khoản hoặc mật khẩu không chính xác`,
                        `Invalid username or password`,
                        401
                    )
                )
            }

            if (user.deletedAt) {
                return next(CustomError(`Người dùng đã bị xóa`, `User has been deleted!`, 401))
            }

            if (!user.isActive) {
                return next(
                    CustomError(
                        `Người dùng tạm thời bị vô hiệu. Vui lòng liên hệ Admin để biết thêm thông tin chi tiết`,
                        `User is temporarily inactive. Please contact Admin for more information`,
                        401
                    )
                )
            }

            // Load user info to request
            req.logIn(user, (err) => {
                if (err) {
                    return next(err)
                }
            })

            return next()
        })(req, res, next)
    }

    const _verifyRole = (role) => (req, res, next) => {
        passport.authenticate('jwt', { session: false }, (err, user, info) => {
            if (err) {
                return next(err)
            }

            if (!user || !Array.isArray(user.roles) || !user.roles.includes(role)) {
                return next(
                    CustomError(
                        `Bạn cần có quyền ${role} để thực hiện thao tác này`,
                        `You are not authorized as ${role}!`,
                        403
                    )
                )
            }

            if (user.deletedAt) {
                return next(CustomError(`Người dùng đã bị xóa`, `User has been deleted!`, 401))
            }

            if (!user.isActive) {
                return next(
                    CustomError(
                        `Người dùng tạm thời bị vô hiệu. Vui lòng liên hệ Admin để biết thêm thông tin chi tiết`,
                        `User is temporarily inactive. Please contact Admin for more information`,
                        401
                    )
                )
            }

            // Load user info to request
            req.logIn(user, (err) => {
                if (err) {
                    return next(err)
                }
            })

            return next()
        })(req, res, next)
    }

    const _loadUserToRequest = (req, res, next) => {
        passport.authenticate('jwt', { session: false }, (err, user, info) => {
            req.user = user
            next()
        })(req, res, next)
    }

    return {
        createToken: _createToken,
        verifyRole: _verifyRole,
        verifyLogin: _verifyLogin,
        loadUserToRequest: _loadUserToRequest,
    }
}
